﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetScoreText : MonoBehaviour {

    private Database m_data;
    public Text m_scoreText;

    void Start() {
        m_data = GameObject.FindObjectOfType<Database>();
        m_scoreText.text = "" + (m_data.GetPoint() + MainGame.instance.GetTime());
    }

}
