﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinScaling : MonoBehaviour {

    public float m_scalingSpeed;
    public float m_scaleLimit;

    private Vector3 m_initScale;
    private RectTransform m_rectTransform;

    void Start() {
        m_rectTransform = GetComponent<RectTransform>();
        m_initScale = m_rectTransform.localScale;
    }


    void Update() {
        m_rectTransform.localScale = m_initScale + new Vector3(Mathf.Sin(Time.time * m_scalingSpeed) * m_scaleLimit, Mathf.Sin(Time.time * m_scalingSpeed) * m_scaleLimit, 0.0f);
    }
}
