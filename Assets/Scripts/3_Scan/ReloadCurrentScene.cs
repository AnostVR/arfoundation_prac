﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ReloadCurrentScene : MonoBehaviour {

    public Fading m_fading;
    public Image m_fadeImage;

    private AsyncOperation m_async;

    public void OnClick() {
        StartCoroutine(ReloadScene());
    }

    private IEnumerator ReloadScene() {
        m_fading.FadeStart(Fading.FadeType.OUT_BLACK, m_fadeImage);
        m_async = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
        m_async.allowSceneActivation = false;
        while(m_async.progress < 0.9f) {
            yield return null;
        }
        yield return new WaitForSeconds(1.5f);
        m_async.allowSceneActivation = true;
    }

    public void Reload() {
        StartCoroutine(ReloadScene());
    }
}
