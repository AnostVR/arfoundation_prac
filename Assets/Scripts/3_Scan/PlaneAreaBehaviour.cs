﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.XR.ARFoundation;

public class PlaneAreaBehaviour : MonoBehaviour {

    public TextMeshPro m_textMeshPro;
    public ARPlane m_arPlane;
    private float m_area;

    private void Start() {
        PlaneCounter planeCounter = GameObject.FindObjectOfType<PlaneCounter>();
        if (planeCounter != null) {
            planeCounter.AddOne();
        } else {
            Debug.LogError("PlaneCounter is Null.");
        }

        AreaChecker areaChecker = GameObject.FindObjectOfType<AreaChecker>();
        if(areaChecker != null) {
            areaChecker.SetPlaneAreaBehaviour(this);
        } else {
            Debug.LogError("AreaChecker is Null");
        }
    }

    private void Update() {
        m_textMeshPro.transform.rotation = Quaternion.LookRotation(m_textMeshPro.transform.position - Camera.main.transform.position);
        ARPlaneBoundaryChanged();
    }

    private void ARPlaneBoundaryChanged() {
        m_area = CalculateArea(m_arPlane);
        m_textMeshPro.text = "" + m_area;
    }

    private float CalculateArea(ARPlane arPlane) {
        return arPlane.size.x * arPlane.size.y;
    }

    public void ToggleAreaView() {
        if (m_textMeshPro.enabled) {
            m_textMeshPro.enabled = false;
        } else {
            m_textMeshPro.enabled = true;
        }
    }

    public float GetArea() {
        return m_area;
    }
}
