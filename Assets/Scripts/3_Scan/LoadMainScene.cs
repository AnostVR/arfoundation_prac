﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class LoadMainScene : MonoBehaviour {

    public string m_nextScene;
    public Fading m_fading;
    public Image m_fadeImage;
    public List<GameObject> m_dontDestroyObjectsList;

    public void OnClick() {
        StartCoroutine(LoadScene());
    }

    private IEnumerator LoadScene() {
        m_fadeImage.raycastTarget = true;
        m_fading.FadeStart(Fading.FadeType.OUT_BLACK, m_fadeImage);
        AsyncOperation async = SceneManager.LoadSceneAsync(m_nextScene);
        async.allowSceneActivation = false;
        AddDontDestroyComponent();
        while(async.progress < 0.9f) {
            yield return null;
        }
        yield return new WaitForSeconds(1.5f);
        async.allowSceneActivation = true;
    }

    private void AddDontDestroyComponent() {
        m_dontDestroyObjectsList.Add(GameObject.FindObjectOfType<ARPlane>().gameObject);
        foreach(var obj in m_dontDestroyObjectsList) {
            obj.AddComponent<DontDestroy>();
        }
    }
}
