﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AreaChecker : MonoBehaviour {

    public float m_threshold;
    public Image m_gaugeImage;
    private float m_area;
    private PlaneAreaBehaviour m_planeAreaBehaviour;
    [SerializeField] private bool m_canLoadNextStep;

    void Update() {
        if (m_planeAreaBehaviour != null) {
            m_area = m_planeAreaBehaviour.GetArea();
            GaugeProgressing();
            CheckCurrentArea();
        }
    }

    private void CheckCurrentArea() {
        if (m_area > m_threshold) {
            m_canLoadNextStep = true;
        } else {
            m_canLoadNextStep = false;
        }
    }

    private void GaugeProgressing() {
        m_gaugeImage.fillAmount = m_area / m_threshold;
    }

    public void SetPlaneAreaBehaviour(PlaneAreaBehaviour planeAreaBehaviour) {
        m_planeAreaBehaviour = planeAreaBehaviour;
    }

    public bool CheckRequirement() {
        return m_canLoadNextStep;
    }
}
