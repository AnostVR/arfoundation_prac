﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneAreaManager : MonoBehaviour {

    private Touch m_touch;
    private Ray m_ray;
    private RaycastHit m_hit;
    private PlaneAreaBehaviour m_planeAreaBehaviour;

    void Update() {
        if(Input.touchCount > 0) {
            m_touch = Input.GetTouch(0);
            if(m_touch.phase == TouchPhase.Ended) {
                if(Input.touchCount == 1) {
                    RayToPlane();
                }
            }
        }
    }

    private void RayToPlane() {
        if (Physics.Raycast(m_ray, out m_hit)) {
            m_planeAreaBehaviour = m_hit.collider.gameObject.GetComponent<PlaneAreaBehaviour>();
            if (m_planeAreaBehaviour != null) {
                m_planeAreaBehaviour.ToggleAreaView();
            }
        }
    }
}
