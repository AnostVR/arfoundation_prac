﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class Judgement : MonoBehaviour {

    public PlaneCounter m_planeCounter;
    public AreaChecker m_areaChecker;
    public ARPlaneManager m_arPlaneManager;
    public GameObject m_scanDoneImageObj;
    public GameObject m_loadNextSceneButtonObj;
    public GameObject m_bgm;
    public bool m_complete = false;

    void Update() {
        if(!m_complete) {
            if (m_planeCounter.CheckRequirement() && m_areaChecker.CheckRequirement()) {
                m_arPlaneManager.enabled = false;
                m_scanDoneImageObj.SetActive(true);
                m_loadNextSceneButtonObj.SetActive(true);
                m_bgm.SetActive(false);
                m_complete = true;
            }
        }
    }
}
