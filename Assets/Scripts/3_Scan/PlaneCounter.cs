﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneCounter : MonoBehaviour {

    private int m_count = 0;
    [SerializeField] private bool m_canLoadNextStep = true;
    public GameObject m_warningObj;

    public void AddOne() {
        m_count++;
        CheckCount();
    }

    private void CheckCount() {
        if (m_count >= 2) {
            m_warningObj.SetActive(true);
            m_canLoadNextStep = false;
        }
    }

    public bool CheckRequirement() {
        return m_canLoadNextStep;
    }
}
