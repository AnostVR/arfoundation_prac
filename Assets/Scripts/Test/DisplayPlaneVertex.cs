﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayPlaneVertex : MonoBehaviour {

    private Mesh m_mesh;
    private Vector3[] m_vertPos;
    private Matrix4x4 m_thisMatrix;
    private GameObject m_pet;
    private float m_AxisXSign = 1.0f;
    private float m_AxisZSign = 1.0f;
    private const float ANGLE_THRESHOLD = 90.0f;

    void Start() {
        Vector3 subPos;
        Vector3 maxPos, minPos;
        float minX, maxX, maxZ, planePosY;
        Vector3 centerPos;
        GameObject cube;
        maxPos = new Vector3(float.MinValue, 0.0f, 0.0f);
        minPos = new Vector3(float.MaxValue, 0.0f, 0.0f);
        minX = float.MaxValue;
        maxX = float.MinValue;
        maxZ = float.MinValue;

        m_mesh = GetComponent<MeshFilter>().mesh;
        m_vertPos = m_mesh.vertices;
        m_thisMatrix = transform.localToWorldMatrix;

        m_pet = GameObject.FindWithTag("Pet");
        if (Vector2.Angle(new Vector2(m_pet.transform.position.x, m_pet.transform.position.z), Vector2.right) > ANGLE_THRESHOLD) {
            m_AxisXSign *= -1.0f;
        }
        if (Vector2.Angle(new Vector2(m_pet.transform.position.x, m_pet.transform.position.z), Vector2.up) > ANGLE_THRESHOLD) {
            m_AxisZSign *= -1.0f;
        }

        centerPos = GetCenterPosition(gameObject.transform);
        cube = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        cube.transform.localPosition = centerPos;
        cube.transform.localScale = Vector3.one * 0.07f;
        for (int i = 0; i < m_vertPos.Length; i++) {
            m_vertPos[i] = m_thisMatrix.MultiplyPoint3x4(m_vertPos[i]);
            subPos = m_vertPos[i] - centerPos;
            subPos = new Vector3(subPos.x * m_AxisXSign, subPos.y, subPos.z * m_AxisZSign);

            if (minX > m_vertPos[i].x) {
                minX = m_vertPos[i].x;
            }
            if (maxX < m_vertPos[i].x) {
                maxX = m_vertPos[i].x;
            }

            if (subPos.z < 0) continue;

            if (maxZ < m_vertPos[i].z) {
                maxZ = m_vertPos[i].z;
            }
        }
        planePosY = m_vertPos[0].y;

        cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.localPosition = new Vector3(minX, planePosY, maxZ);
        cube.transform.localScale = Vector3.one * 0.05f;

        cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.localPosition = new Vector3(maxX, planePosY, maxZ);
        cube.transform.localScale = Vector3.one * 0.05f;

    }

    public Vector3 GetCenterPosition(Transform target) {
        //非アクティブも含めて、targetとtargetの子全てのレンダラーとコライダーを取得
        var cols = target.GetComponentsInChildren<Collider>(true);
        var rens = target.GetComponentsInChildren<Renderer>(true);

        //コライダーとレンダラーが１つもなければ、target.positionがcenterになる
        if (cols.Length == 0 && rens.Length == 0)
            return target.position;

        bool isInit = false;

        Vector3 minPos = Vector3.zero;
        Vector3 maxPos = Vector3.zero;

        for (int i = 0; i < cols.Length; i++) {
            var bounds = cols[i].bounds;
            var center = bounds.center;
            var size = bounds.size / 2;

            //最初の１度だけ通って、minPosとmaxPosを初期化する
            if (!isInit) {
                minPos.x = center.x - size.x;
                minPos.y = center.y - size.y;
                minPos.z = center.z - size.z;
                maxPos.x = center.x + size.x;
                maxPos.y = center.y + size.y;
                maxPos.z = center.z + size.z;

                isInit = true;
                continue;
            }

            if (minPos.x > center.x - size.x) minPos.x = center.x - size.x;
            if (minPos.y > center.y - size.y) minPos.y = center.y - size.y;
            if (minPos.z > center.z - size.z) minPos.z = center.z - size.z;
            if (maxPos.x < center.x + size.x) maxPos.x = center.x + size.x;
            if (maxPos.y < center.y + size.y) maxPos.y = center.y + size.y;
            if (maxPos.z < center.z + size.z) maxPos.z = center.z + size.z;
        }
        for (int i = 0; i < rens.Length; i++) {
            var bounds = rens[i].bounds;
            var center = bounds.center;
            var size = bounds.size / 2;

            //コライダーが１つもなければ１度だけ通って、minPosとmaxPosを初期化する
            if (!isInit) {
                minPos.x = center.x - size.x;
                minPos.y = center.y - size.y;
                minPos.z = center.z - size.z;
                maxPos.x = center.x + size.x;
                maxPos.y = center.y + size.y;
                maxPos.z = center.z + size.z;

                isInit = true;
                continue;
            }

            if (minPos.x > center.x - size.x) minPos.x = center.x - size.x;
            if (minPos.y > center.y - size.y) minPos.y = center.y - size.y;
            if (minPos.z > center.z - size.z) minPos.z = center.z - size.z;
            if (maxPos.x < center.x + size.x) maxPos.x = center.x + size.x;
            if (maxPos.y < center.y + size.y) maxPos.y = center.y + size.y;
            if (maxPos.z < center.z + size.z) maxPos.z = center.z + size.z;
        }

        return (minPos + maxPos) / 2;
    }
}
