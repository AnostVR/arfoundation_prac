﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class AddPointCloudComp : MonoBehaviour {

    private GameObject m_pointCloud;

    void Start() {
        var ff = GameObject.FindObjectOfType<ARSessionOrigin>().gameObject.AddComponent<ARPointCloudManager>();
        m_pointCloud = Resources.Load("PC") as GameObject;
        if(ff != null) {
            ff.pointCloudPrefab = m_pointCloud;
        }
    }

    void Update() {
        
    }
}
