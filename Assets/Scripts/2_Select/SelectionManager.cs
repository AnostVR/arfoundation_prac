﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class SelectionManager : MonoBehaviour {

    public GameObject[] m_statusObjects;
    public Database m_data;
    [SerializeField] private Transform m_cameraTransform;
    [SerializeField] private int m_index;
    private string m_findTag = "Selected";
    private Text m_characterNameText;
    private const int MIN_INDEX = 0;
    private const int MAX_INDEX = 2;
    private const float REGULAR_DISTANCE_X = 100.0f;
    private Vector3 REGULAR_DISTANCE;

    void Start() {
        REGULAR_DISTANCE = new Vector3(REGULAR_DISTANCE_X, 0.0f, 0.0f);
        m_index = MIN_INDEX;
        m_characterNameText = GameObject.FindWithTag(m_findTag).GetComponent<Text>();
        m_data.SetPet(DeleteHTMLTag(m_characterNameText.text));
    }

    private void StatusActivate(int index) {
        for (int i = 0; i < m_statusObjects.Length; i++) {
            if (m_index == i) {
                m_statusObjects[i].SetActive(true);
            } else {
                m_statusObjects[i].SetActive(false);
            }
        }
    }

    private void CheckSelectedCharacter() {
        m_characterNameText = GameObject.FindWithTag(m_findTag).GetComponent<Text>();
        m_data.SetPet(DeleteHTMLTag(m_characterNameText.text));
    }

    private string DeleteHTMLTag(string txt) {
        return Regex.Replace(txt, "<[^>]*?>", string.Empty);
    }

    public void OnClick(bool isRight) {
        if(isRight) {
            if(m_index == MAX_INDEX) {
                m_index = MIN_INDEX;
            } else {
                m_index++;
            }
        } else {
            if(m_index == MIN_INDEX) {
                m_index = MAX_INDEX;
            } else {
                m_index--;
            }
        }
        m_cameraTransform.position = REGULAR_DISTANCE * m_index;
        StatusActivate(m_index);
        CheckSelectedCharacter();
    }
}
