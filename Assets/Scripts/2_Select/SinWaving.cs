﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinWaving : MonoBehaviour {

    [SerializeField] private Transform m_cameraTransform;
    [SerializeField] private float m_moveSpeed;
    [SerializeField] private float m_distance;
    [SerializeField] private bool m_inverse;
    private RectTransform m_rectTransform;
    private Vector3 m_initPos;
    private const float INVERSE = -1.0f;

    void Start() {
        m_rectTransform = GetComponent<RectTransform>();
        m_initPos = m_rectTransform.position;
        if(m_inverse) {
            m_distance *= INVERSE;
        }
    }

    void Update() {
        m_rectTransform.position = m_cameraTransform.position + m_initPos + new Vector3(Mathf.Sin(Time.time * m_moveSpeed) * m_distance, 0.0f, 0.0f);
    }
}
