﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectCharacter : MonoBehaviour {

    public GameObject m_confirmationPanel;
    public Text m_selectedPetNameText;
    public Database m_data;

    public void OnClick() {
        m_confirmationPanel.SetActive(true);
        m_selectedPetNameText.text = m_data.GetPet().name;
    }
}
