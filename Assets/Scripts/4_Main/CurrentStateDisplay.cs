﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentStateDisplay : MonoBehaviour {

    private Text m_skipText;
    private const string WAVE_TEXT = "WAVE ";
    private Color WAVE_TEXT_COLOR = new Color(46.0f / 255.0f, 154.0f / 255.0f, 254.0f / 255.0f);
    private WaveImageAnimation m_waveImageAnimation;
    public void Init(Text skipText) {
        m_skipText = skipText;
        m_skipText.text = WAVE_TEXT + MainGame.instance.GetCurrentWave();
        m_skipText.color = WAVE_TEXT_COLOR;
        Outline outline = m_skipText.GetComponent<Outline>();
        outline.effectColor = Color.white;
    }

    public void DisplayCurrentWave() {
        m_skipText.text = WAVE_TEXT + MainGame.instance.GetCurrentWave();
    }
}
