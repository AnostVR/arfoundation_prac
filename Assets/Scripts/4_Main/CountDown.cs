﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDown : MonoBehaviour {

    public GameObject m_countDownUI;
    public Image m_circleImage;
    public Text m_countDownText;

    private const float WAIT_FOR_ONE_SEC = 1.0f;
    private const float WAIT_FOR_START = 3.0f;

    public IEnumerator GameStart() {
        float time = WAIT_FOR_ONE_SEC;
        int count = 3;
        yield return new WaitForSeconds(WAIT_FOR_ONE_SEC);
        m_countDownUI.SetActive(true);
        while(count != 0) {
            m_circleImage.fillAmount = time;
            time -= Time.deltaTime;
            if(time < 0) {
                time = WAIT_FOR_ONE_SEC;
                count--;
                if(count == 0) {
                    m_countDownUI.SetActive(false);
                    break;
                }
            }
            m_countDownText.text = string.Format("{0}", count);
            yield return null;
        }
        MainGame.instance.m_isPlaying = true;
    }
}
