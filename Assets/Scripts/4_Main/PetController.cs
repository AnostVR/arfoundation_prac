﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class PetController : MonoBehaviour {

    public bool m_canControl = false;
    public bool[] m_jumpPermissions;

    private ARRaycastManager m_arRaycastManager;
    private List<ARRaycastHit> m_hit = new List<ARRaycastHit>();
    private Pose m_pose;

    private int m_hp;
    private float m_moveSpeed;
    private float m_flightDuration = 0.0f;
    private float m_jumpSpeed;
    private const float FLIGHT_SPEED = 20.0f;
    private Rigidbody m_rigidbody;
    private bool m_canDoubleJump;
    private Coroutine m_doubleJumpCoroutine;

    private Vector3 m_touchPos;

    private Transform m_targetTransform;
    private Vector3 m_direction;
    private float m_distance;
    private const float MIN_DISTANCE = 0.01f;

    private SceneLoader m_sceneLoader;
    private GameObject m_eff_gameover;

    private const float DAMAGE_SPAN = 1.0f;
    private bool m_canDamage;

    private AudioSource m_audioSource;
    private AudioClip m_audioClip;

    private HPImageController m_hpImageController;
    private const float DELAY = 1.0f;

    void Start() {
        m_jumpPermissions = new bool[2];
        m_rigidbody = GetComponent<Rigidbody>();
        Database database = GameObject.FindObjectOfType<Database>();
        PetsType petType = database.GetPet();
        m_hp = petType.hp;
        m_moveSpeed = petType.moveSpeed;
        m_jumpSpeed = petType.jumpSpeed;
        m_flightDuration = petType.flightDutation;
        if (m_flightDuration > 0.0f) {
            m_canDoubleJump = true;
        }
        m_targetTransform = GameObject.FindWithTag("Target").transform;

        m_arRaycastManager = GameObject.FindObjectOfType<ARRaycastManager>();
        m_sceneLoader = GameObject.FindObjectOfType<SceneLoader>();
        m_eff_gameover = Resources.Load("eff_gameover") as GameObject;
        m_canDamage = true;

        m_audioSource = GetComponent<AudioSource>();
        m_audioSource.clip = Resources.Load("se_kick1") as AudioClip;
    }

    void Update() {
        if(m_canControl) {
            TargetMove();
            PetMove();

            if (m_hp <= 0) {
                StartCoroutine(GameOver());
            }
        }
    }

    private IEnumerator GameOver() {
        m_canControl = false;
        transform.GetChild(0).gameObject.SetActive(false);
        Instantiate(m_eff_gameover, transform.position, Quaternion.identity);
        MainGame.instance.m_isPlaying = false;
        yield return new WaitForSeconds(DELAY);
        StartCoroutine(m_sceneLoader.LoadGameOverScene());
    }

    private void TargetMove() {
        if (Input.touchCount > 0) {
            m_jumpPermissions[1] = true;
            m_touchPos = Input.GetTouch(0).position;
            if (m_arRaycastManager.Raycast(m_touchPos, m_hit, TrackableType.Planes)) {
                m_pose = m_hit[0].pose;
                m_targetTransform.position = m_pose.position;
            }

        } else if (Input.touchCount == 0) {
            Jump();
        }

    }

    private void PetMove() {
        m_direction = m_moveSpeed * (m_targetTransform.position - transform.position).normalized;
        m_distance = Vector3.Distance(m_targetTransform.position, transform.position);
        if(m_distance > MIN_DISTANCE) {
            m_rigidbody.AddForce(m_direction);
        }
    }

    public void Jump() {
        if (m_jumpPermissions[0] && m_jumpPermissions[1]) {
            m_rigidbody.AddForce(transform.up * m_jumpSpeed);
            ChangeJumpPermission(false);
            if (m_canDoubleJump) {
                m_doubleJumpCoroutine = StartCoroutine(DoubleJump());
            }
        }
    }

    public IEnumerator DoubleJump() {
        yield return new WaitForSeconds(m_flightDuration);
        Flight();
    }

    public void Flight() {
        m_rigidbody.AddForce(transform.up * FLIGHT_SPEED);
    }

    public void Dive() {
        if(m_doubleJumpCoroutine != null) {
            StopCoroutine(m_doubleJumpCoroutine);
        }
        m_rigidbody.velocity = Vector3.down * m_jumpSpeed;
    }

    private void OnCollisionEnter(Collision collision) {
        if(collision.gameObject.CompareTag("Ground")) {
            m_jumpPermissions[0] = true;
        }
    }

    private void OnTriggerEnter(Collider other) {
        if(other.tag == "Obstacle") {
            if(m_canDamage) {
                m_canDamage = false;
                m_hp--;
                m_audioSource.Play();
                m_hpImageController.DestroyOneHeart();
                StartCoroutine(WaitForInvinciblyTime());
            }
        }
    }

    private IEnumerator WaitForInvinciblyTime() {
        yield return new WaitForSeconds(DAMAGE_SPAN);
        m_canDamage = true;
    }

    public int GetHP() {
        return m_hp;
    }

    public void InitHPImage() {
        m_hpImageController = GameObject.FindObjectOfType<HPImageController>();
        m_hpImageController.Init();
    }

    #region Permission
    public void ToggleControlPermission() {
        m_canControl = !m_canControl;
    }

    public void ChangeJumpPermission(bool permission) {
        m_jumpPermissions[0] = m_jumpPermissions[1] = permission;
    }
    #endregion
}
