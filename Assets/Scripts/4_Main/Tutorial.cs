﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class Tutorial : MonoBehaviour {

    private Canvas m_canvas;
    private ARRaycastManager m_arRaycastManager;
    private List<ARRaycastHit> m_hit = new List<ARRaycastHit>();
    private int m_phase = 0;
    private Vector2 m_touchPos;
    private Pose m_pose;
    private GameObject m_spawnedObject;
    private GameObject m_selectedPetObj;
    private GameObject m_pet;
    private PetController m_controller;

    private const float GAUGE_TOUCH_NORM_TIME = 2.0f;
    private const float MOVE_TUTORIAL_NORM_TIME = 1.0f;
    private float m_time = 0;

    public GameObject m_target;
    public Image m_gaugeImage;
    public Text m_guideText;
    private Vector3 m_direction;
    private const float MOVE_SPEED = 1.0f;
    private const float ANGLE_ADJUSTER = 0.0f;
    private const float SCALE_ADJUSTER = 0.1f;
    private Vector3 INIT_POSITION_ADJUSTER = new Vector3(0.0f, 0.3f, 0.0f);
    [Multiline] public string m_initMessage;
    [Multiline] public string m_petComingMessage;
    [Multiline] public string m_jumpingTutorialMessage;
    [Multiline] public string m_moveTutorialMessage;
    [Multiline] public string m_completeMessage;
    [Multiline] public string m_errorMessage;

    public Fading m_fading;
    public Image m_fadeImage;

    private float m_planePosY;
    private const float RELOAD_POS_Y = -0.02f;
    private const float WAIT_FOR_FADE = 2.0f;
    private bool m_reloading;

    public GameObject m_mainGameObj;
    public Text m_skipText;

    void Start() {
        m_canvas = GameObject.FindObjectOfType<Canvas>();
        m_canvas.worldCamera = Camera.main;
        m_arRaycastManager = GameObject.FindObjectOfType<ARSessionOrigin>().gameObject.AddComponent<ARRaycastManager>();

        m_selectedPetObj = Resources.Load(GameObject.FindObjectOfType<Database>().GetPet().name) as GameObject;
        m_skipText.gameObject.SetActive(false);

        // TextMeshProの削除
        Destroy(GameObject.FindObjectOfType<ARPlane>().transform.GetChild(0).gameObject);
    }

    void Update() {
        if (Input.touchCount > 0) {
            m_touchPos = Input.GetTouch(0).position;
            if (m_arRaycastManager.Raycast(m_touchPos, m_hit, TrackableType.Planes)) {
                m_pose = m_hit[0].pose;

                switch (m_phase) {
                    case 0:
                        Init();
                        break;

                    case 1:
                        TouchTutorial();
                        break;

                    case 2:
                        m_phase = -1;
                        StartCoroutine(PetComing());
                        break;

                    case 3:
                        m_phase = -1;
                        StartCoroutine(JumpingTutorial());
                        break;

                    case 4:
                        break;

                    case 5:
                        MoveTutorialInit();
                        break;

                    case 6:
                        MoveTutorial();
                        break;

                    case 7:
                        m_phase = -1;
                        StartCoroutine(TutorialComplete());
                        break;
                }
            }
        } else if(Input.touchCount == 0) {            
            if (m_phase == 4) {
                m_phase = -1;
                StartCoroutine(Jump());
            } else if(m_phase == 6) {
                m_controller.Jump();
            }
        }

        if(m_pet != null) {
            if(m_pet.transform.position.y < RELOAD_POS_Y + m_planePosY) {
                if (!m_reloading) {
                    m_reloading = true;
                    StartCoroutine(PhaseReset());
                }
            }
        }
    }

    private IEnumerator PhaseReset() {
        m_skipText.gameObject.SetActive(false);
        m_guideText.text = m_errorMessage;
        m_fadeImage.raycastTarget = true;
        m_phase = -1;
        yield return new WaitForSeconds(WAIT_FOR_FADE);
        m_fading.FadeStart(Fading.FadeType.OUT_BLACK, m_fadeImage);
        yield return new WaitForSeconds(WAIT_FOR_FADE);

        Destroy(m_pet.gameObject);
        Destroy(m_spawnedObject.gameObject);
        m_planePosY = 0.0f;
        m_phase = 0;
        m_time = 0;
        m_gaugeImage.transform.parent.gameObject.SetActive(true);
        m_gaugeImage.fillAmount = 0.0f;
        m_guideText.text = m_initMessage;
        m_controller = null;
        m_fading.FadeStart(Fading.FadeType.IN_BLACK, m_fadeImage);
        yield return new WaitForSeconds(WAIT_FOR_FADE);
        m_reloading = false;
        m_fadeImage.raycastTarget = false;
    }

    private void Init() {
        m_spawnedObject = Instantiate(m_target, m_pose.position, Quaternion.identity);
        m_planePosY = m_pose.position.y;
        m_phase++;
    }

    private void TouchTutorial() {
        m_spawnedObject.transform.position = m_pose.position;
        m_time += Time.deltaTime;
        m_gaugeImage.fillAmount = m_time / GAUGE_TOUCH_NORM_TIME;
        if(m_time > GAUGE_TOUCH_NORM_TIME) {
            m_gaugeImage.transform.parent.gameObject.SetActive(false);
            m_phase++;
        }
    }

    private IEnumerator PetComing() {
        Vector3 petInitPos = m_pose.position + INIT_POSITION_ADJUSTER;
        m_pet = Instantiate(m_selectedPetObj, petInitPos, Quaternion.identity);
        m_pet.transform.localScale = m_pet.transform.localScale * SCALE_ADJUSTER;
        m_pet.GetComponent<Rigidbody>().useGravity = true;

        m_direction = Camera.main.transform.forward - m_pet.transform.position;
        m_direction.Normalize();
        m_guideText.text = m_petComingMessage;
        //m_skipText.gameObject.SetActive(true);
        while (Vector3.Angle(Camera.main.transform.forward, m_pet.transform.forward) == ANGLE_ADJUSTER) {
            m_pet.transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(m_direction), MOVE_SPEED * Time.deltaTime);
            yield return null;
        }
        m_phase = 3;
    }

    private IEnumerator JumpingTutorial() {
        m_controller = m_pet.AddComponent<PetController>();
        m_guideText.text = m_jumpingTutorialMessage;
        yield return new WaitForSeconds(0.1f);
        m_phase = 4;
    }

    private IEnumerator Jump() {
        m_controller.ChangeJumpPermission(true);
        m_controller.Jump();
        yield return new WaitForSeconds(0.5f);
        m_phase = 5;
    }

    private void MoveTutorialInit() {
        m_controller.ToggleControlPermission();
        m_guideText.text = m_moveTutorialMessage;
        m_time = 0;
        m_phase = 6;
    }

    private void MoveTutorial() {
        m_spawnedObject.transform.position = m_pose.position;
        m_time += Time.deltaTime;

        if(m_time > MOVE_TUTORIAL_NORM_TIME) {
            m_phase = 7;
            m_time = 0.0f;
        }
    }

    public IEnumerator TutorialComplete() {
        m_guideText.text = m_completeMessage;
        m_fadeImage.raycastTarget = true;
        yield return new WaitForSeconds(WAIT_FOR_FADE);
        m_fading.FadeStart(Fading.FadeType.OUT_BLACK, m_fadeImage);
        yield return new WaitForSeconds(WAIT_FOR_FADE);
        GameObject mainObj = Instantiate(m_mainGameObj, m_pose.position, Quaternion.identity);
        m_skipText.gameObject.SetActive(true);
        m_skipText.text = "";
        m_skipText.GetComponent<EventTrigger>().enabled = false;
        GameObject.FindObjectOfType<CurrentStateDisplay>().Init(m_skipText);
        //Destroy(m_guideText.gameObject);
        Destroy(gameObject);
    }

    public void SkipTutorial() {
        StartCoroutine(TutorialComplete());
    }
}