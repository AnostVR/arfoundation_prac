﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveImageAnimation : MonoBehaviour {

    private float m_time;
    private Image m_waveImage;

    private const float ONE_SEC = 1.0f;
    private const float FILL_AMOUNT_MAX = 1.0f;
    private const float FILL_AMOUNT_MIN = 0.0f;
    private const float FILL_AMOUNT_SPEED = 1.0f;

    private bool m_canNextWave = true;

    private void Start() {
        m_waveImage = GetComponent<Image>();
    }

    public void StartAnimation() {
        if (m_canNextWave) {
            m_canNextWave = false;
            StartCoroutine(ImageAnimation());
        }
    }

    public IEnumerator ImageAnimation() {
        m_time = 0.0f;
        m_waveImage.fillAmount = FILL_AMOUNT_MIN;

        m_waveImage.fillOrigin = (int)Image.OriginHorizontal.Left;
        while (m_waveImage.fillAmount != FILL_AMOUNT_MAX) {
            m_waveImage.fillAmount += FILL_AMOUNT_SPEED * Time.deltaTime;
            yield return null;
        }
        yield return new WaitForSeconds(ONE_SEC);
        m_waveImage.fillOrigin = (int)Image.OriginHorizontal.Right;
        while (m_waveImage.fillOrigin != FILL_AMOUNT_MIN) {
            m_waveImage.fillAmount -= FILL_AMOUNT_SPEED * Time.deltaTime;
            yield return null;
        }
        m_waveImage.fillOrigin = (int)Image.OriginHorizontal.Left;
        m_canNextWave = true;
    }
}