﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class MainGame : MonoBehaviour {

    public static MainGame instance;

    private int m_currentWave = 1;
    public bool m_isPlaying;
    private Image m_fadeImage;
    private Fading m_fading;
    private CountDown m_countDown;
    private float m_timer = 0;
    private Spawner m_spawner;
    private bool m_canSpawnWave;
    private CurrentStateDisplay m_currentStateDisplay;
    private WaveImageAnimation m_waveImageAnimation;

    private const int SPAWN_ADJUSTMENT = 2;
    private const int CORRECTION_VALUE = 5;
    private const int INDEMNIFICATION = 2;
    private const float BASE_TIME = 2.2f;
    private const float DECELERATION_TIME = 0.2f;

    private void Awake() {
        if(instance == null) {
            instance = this;
        } else if(instance != null) {
            Destroy(this.gameObject);
        }
    }

    void Start() {
        m_fading = GameObject.FindObjectOfType<Fading>();
        m_fadeImage = m_fading.GetComponent<Image>();
        m_fading.FadeStart(Fading.FadeType.IN_BLACK, m_fadeImage);
        m_countDown = GameObject.FindObjectOfType<CountDown>();
        StartCoroutine(m_countDown.GameStart());
        GameObject spawnerObj = Instantiate(Resources.Load("Spawner") as GameObject);
        m_spawner = spawnerObj.GetComponent<Spawner>();
        m_canSpawnWave = true;
        PetController petController = GameObject.FindObjectOfType<PetController>();
        petController.InitHPImage();
        m_currentStateDisplay = GameObject.FindObjectOfType<CurrentStateDisplay>();
        m_waveImageAnimation = GameObject.FindObjectOfType<WaveImageAnimation>();
        DontDestroyOnLoad(this.gameObject);
    }

    void Update() {
        if(m_isPlaying) {
            m_timer += Time.deltaTime;
            if(m_canSpawnWave) {
                m_canSpawnWave = false;
                GameObject waveObj = new GameObject();
                Wave wave = waveObj.AddComponent<Wave>();
                wave.Init(
                    m_spawner,
                    m_currentWave * CORRECTION_VALUE / SPAWN_ADJUSTMENT + INDEMNIFICATION,
                    BASE_TIME - DECELERATION_TIME * m_currentWave
                );
            }
        }
    }

    public int GetCurrentWave() {
        return m_currentWave;
    }
    public void NextWave() {
        m_currentWave++;
        m_currentStateDisplay.DisplayCurrentWave();
        StartCoroutine(m_waveImageAnimation.ImageAnimation());
    }

    public void GivePermission() {
        m_canSpawnWave = true;
    }

    public int GetTime() {
        return (int)m_timer;
    }
}
