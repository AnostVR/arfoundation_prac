﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ScrollGround : MonoBehaviour {

    private Vector2 m_petVector;
    private Material m_matGround;
    private float m_groundMoveSpeed;
    private const float INIT_GROUND_MOVE_SPEED = 0.5f;

    void Start() {
        m_matGround = GameObject.FindObjectOfType<ARPlane>().GetComponent<MeshRenderer>().material;
        GameObject pet = GameObject.FindWithTag("Pet");
        m_petVector = new Vector2(pet.transform.position.x, pet.transform.position.z);
        m_petVector.Normalize();
        m_groundMoveSpeed = INIT_GROUND_MOVE_SPEED;
    }

    void Update() {
        if(MainGame.instance.m_isPlaying) {
            m_matGround.mainTextureOffset += m_petVector * m_groundMoveSpeed * Time.deltaTime;
        }
    }

    public void SpeedChange(float value) {
        m_groundMoveSpeed += value;
    }

    public Vector2 GetScrollVector() {
        return m_petVector;
    }

    public float GetGroundMoveSpeed() {
        return m_groundMoveSpeed;
    }
}
