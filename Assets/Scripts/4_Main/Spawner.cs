﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class Spawner : MonoBehaviour {

    private float m_minX;
    private float m_maxX;
    private float m_maxZ;
    private float m_planePosY;
    private GameObject m_pet;

    public GameObject[] m_obstacles;

    void Start() {
        CalcSpawnPos();
    }
    public void Spawn() {
        Instantiate(
            m_obstacles[Random.Range(0, m_obstacles.Length)],
            GetSpawnPosition(),
            Quaternion.identity
        );
    }

    private void CalcSpawnPos() {
        Vector3 subPos;
        Vector3 centerPos;
        Matrix4x4 thisMatrix;
        Vector3[] vertPos;
        ARPlane arPlane;
        float axisXSign = 1.0f;
        float axisZSign = 1.0f;
        const float ANGLE_THRESHOLD = 90.0f;

        m_minX = float.MaxValue;
        m_maxX = float.MinValue;
        m_maxZ = float.MinValue;
        
        m_pet = GameObject.FindWithTag("Pet");
        arPlane = GameObject.FindObjectOfType<ARPlane>();
        vertPos = arPlane.gameObject.GetComponent<MeshFilter>().mesh.vertices;
        thisMatrix = arPlane.gameObject.transform.localToWorldMatrix;
        if (Vector2.Angle(new Vector2(m_pet.transform.position.x, m_pet.transform.position.z), Vector2.right) > ANGLE_THRESHOLD) {
            axisXSign *= -1.0f;
        }
        if (Vector2.Angle(new Vector2(m_pet.transform.position.x, m_pet.transform.position.z), Vector2.up) > ANGLE_THRESHOLD) {
            axisZSign *= -1.0f;
        }

        centerPos = GetCenterPosition(arPlane.gameObject.transform);
        for (int i = 0; i < vertPos.Length; i++) {
            vertPos[i] = thisMatrix.MultiplyPoint3x4(vertPos[i]);
            subPos = vertPos[i] - centerPos;
            subPos = new Vector3(subPos.x * axisXSign, subPos.y, subPos.z * axisZSign);

            if (m_minX > vertPos[i].x) {
                m_minX = vertPos[i].x;
            }
            if (m_maxX < vertPos[i].x) {
                m_maxX = vertPos[i].x;
            }

            if (subPos.z < 0) continue;
            if (m_maxZ < vertPos[i].z) {
                m_maxZ = vertPos[i].z;
            }
        }
        m_planePosY = vertPos[0].y;
    }

    public Vector3 GetSpawnPosition() {
        if (Random.Range(0, 2) == 1) {
            return new Vector3(Random.Range(m_minX, m_maxX), m_planePosY, m_maxZ);
        } else {
            return new Vector3(m_pet.transform.position.x, m_planePosY, m_maxZ);
        }
    }

    public Vector3 GetCenterPosition(Transform target) {
        //非アクティブも含めて、targetとtargetの子全てのレンダラーとコライダーを取得
        var cols = target.GetComponentsInChildren<Collider>(true);
        var rens = target.GetComponentsInChildren<Renderer>(true);

        //コライダーとレンダラーが１つもなければ、target.positionがcenterになる
        if (cols.Length == 0 && rens.Length == 0)
            return target.position;

        bool isInit = false;

        Vector3 minPos = Vector3.zero;
        Vector3 maxPos = Vector3.zero;

        for (int i = 0; i < cols.Length; i++) {
            var bounds = cols[i].bounds;
            var center = bounds.center;
            var size = bounds.size / 2;

            //最初の１度だけ通って、minPosとmaxPosを初期化する
            if (!isInit) {
                minPos.x = center.x - size.x;
                minPos.y = center.y - size.y;
                minPos.z = center.z - size.z;
                maxPos.x = center.x + size.x;
                maxPos.y = center.y + size.y;
                maxPos.z = center.z + size.z;

                isInit = true;
                continue;
            }

            if (minPos.x > center.x - size.x) minPos.x = center.x - size.x;
            if (minPos.y > center.y - size.y) minPos.y = center.y - size.y;
            if (minPos.z > center.z - size.z) minPos.z = center.z - size.z;
            if (maxPos.x < center.x + size.x) maxPos.x = center.x + size.x;
            if (maxPos.y < center.y + size.y) maxPos.y = center.y + size.y;
            if (maxPos.z < center.z + size.z) maxPos.z = center.z + size.z;
        }
        for (int i = 0; i < rens.Length; i++) {
            var bounds = rens[i].bounds;
            var center = bounds.center;
            var size = bounds.size / 2;

            //コライダーが１つもなければ１度だけ通って、minPosとmaxPosを初期化する
            if (!isInit) {
                minPos.x = center.x - size.x;
                minPos.y = center.y - size.y;
                minPos.z = center.z - size.z;
                maxPos.x = center.x + size.x;
                maxPos.y = center.y + size.y;
                maxPos.z = center.z + size.z;

                isInit = true;
                continue;
            }

            if (minPos.x > center.x - size.x) minPos.x = center.x - size.x;
            if (minPos.y > center.y - size.y) minPos.y = center.y - size.y;
            if (minPos.z > center.z - size.z) minPos.z = center.z - size.z;
            if (maxPos.x < center.x + size.x) maxPos.x = center.x + size.x;
            if (maxPos.y < center.y + size.y) maxPos.y = center.y + size.y;
            if (maxPos.z < center.z + size.z) maxPos.z = center.z + size.z;
        }

        return (minPos + maxPos) / 2;
    }
}
