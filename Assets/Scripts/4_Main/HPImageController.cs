﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPImageController : MonoBehaviour {

    private GameObject m_heartImageObj;
    private PetController m_petController;
    private List<GameObject> m_displayHeartObjList;
        
    public void Init() {
        m_displayHeartObjList = new List<GameObject>();
        m_petController = GameObject.FindObjectOfType<PetController>();
        Transform childTransform = transform.GetChild(0);
        childTransform.gameObject.SetActive(true);
        m_heartImageObj = childTransform.GetChild(0).gameObject;
        m_displayHeartObjList.Add(m_heartImageObj);
        GameObject heart;
        for(int i = 1; i < m_petController.GetHP(); i++) {
            heart = Instantiate(m_heartImageObj);
            heart.transform.parent = childTransform;
            heart.transform.localPosition = Vector3.zero;
            heart.transform.localScale = m_heartImageObj.transform.localScale;
            m_displayHeartObjList.Add(heart);
        }
    }

    public void DestroyOneHeart() {
        Destroy(m_displayHeartObjList[m_petController.GetHP()]);
    }
}
