﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : Obstacle {

    private float m_moveSpeed;
    private const float LIFE_SPAN = 3.0f;
    private Transform m_petTransform;

    void Start() {
        m_petTransform = GameObject.FindWithTag("Pet").transform;
        transform.LookAt(m_petTransform.position);
        ScrollGround scrollGround = GameObject.FindObjectOfType<ScrollGround>();
        m_moveSpeed = scrollGround.GetGroundMoveSpeed();
        SetLifeSpan();
    }

    void Update() {
        Move();
    }

    protected override void Move() {
        transform.position += transform.forward * m_moveSpeed * Time.deltaTime;
    }

    protected override void SetLifeSpan() {
        Destroy(this.gameObject, LIFE_SPAN);
    }
}
