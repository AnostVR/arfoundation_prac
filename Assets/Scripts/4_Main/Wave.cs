﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Wave : MonoBehaviour {

    private Spawner m_spawner;
    private int m_spawnCount;
    private int m_currentCount = 0;
    private float m_time = 0;
    private float m_spawnSpan;
    //private WaveImageAnimation m_waveImageAnimation;
    private const float WAIT_FOR_LAST_OBSTACLE = 4.0f;

    void Start() {
        //m_waveImageAnimation = GameObject.FindObjectOfType<WaveImageAnimation>();
    }

    void Update() {
        m_time += Time.deltaTime;
        if (m_time > m_spawnSpan) {
            m_time = 0.0f;
            if (m_currentCount > m_spawnCount) {
                m_spawnSpan = float.MaxValue;
                StartCoroutine(Dispose());
            } else {
                SpawnOneObstacle();
            }
        }
    }

    private void SpawnOneObstacle() {
        m_spawner.Spawn();
        m_currentCount++;
    }

    private IEnumerator Dispose() {
        // m_waveImageAnimation.NextWave();
        yield return new WaitForSeconds(WAIT_FOR_LAST_OBSTACLE);
        MainGame.instance.NextWave();
        MainGame.instance.GivePermission();

        //  -------------
        // var debug = GameObject.Find("txt_guide");
        // debug.GetComponent<Text>().text = "ここまできたよ！はかちぇ！";
        //  -------------

        Destroy(this.gameObject);
    }

    public void Init(Spawner spawner, int spawnCount, float spawnSpan) {
        m_spawner = spawner;
        m_spawnCount = spawnCount;
        m_spawnSpan = spawnSpan;
    }

}
