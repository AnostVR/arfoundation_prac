﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pivot : MonoBehaviour {

    [SerializeField] private float m_rotateSpeed;

    void Start() {
        
    }

    void Update() {
        transform.Rotate(new Vector3(0.0f, m_rotateSpeed, 0.0f) * Time.deltaTime);
    }
}
