﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ARLightEffect : MonoBehaviour {

    public ARCameraManager m_arCameraManager;
    private Light m_light;
    private Color m_color;
    private Color m_resultColor;
    private float m_intensity = 1.0f;
    private const float TWICE = 2.0f;

    void Start() {
        m_light = GetComponent<Light>();
        m_color = Color.white;
        m_arCameraManager.frameReceived += OnChangedFrame;
    }
    
    private void OnChangedFrame(ARCameraFrameEventArgs frameEvent) {
        if(frameEvent.lightEstimation.averageBrightness.HasValue) {
            m_intensity = frameEvent.lightEstimation.averageBrightness.Value;
            m_intensity *= TWICE;
            if(m_intensity > 1.0f) {
                m_intensity = 1.0f;
            }

            if(frameEvent.lightEstimation.averageColorTemperature.HasValue) {
                m_color = Mathf.CorrelatedColorTemperatureToRGB(frameEvent.lightEstimation.averageColorTemperature.Value);
            }
            m_resultColor = m_color * m_intensity;
            m_light.color = m_resultColor;
            RenderSettings.ambientSkyColor = m_resultColor;
        }
    }
}
