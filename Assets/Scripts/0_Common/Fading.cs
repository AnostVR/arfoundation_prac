﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fading : MonoBehaviour {

    [SerializeField] private bool m_initFadein;
    private float m_alpha;
    private float m_blackOrWhite;
    public enum FadeType {
        IN_BLACK,
        OUT_BLACK,
        IN_WHITE,
        OUT_WHITE
    };

    void Start() {
        if(m_initFadein) {
            Image img = GetComponent<Image>();
            FadeStart(FadeType.IN_BLACK, img);
        }
    }

    public void FadeStart(FadeType fadeType, Image fadeImage) {
        switch (fadeType) {
            case FadeType.IN_BLACK:
                StartCoroutine(Fadein(fadeImage, 0.0f));
                break;

            case FadeType.IN_WHITE:
                StartCoroutine(Fadein(fadeImage, 1.0f));
                break;

            case FadeType.OUT_BLACK:
                StartCoroutine(Fadeout(fadeImage, 0.0f));
                break;

            case FadeType.OUT_WHITE:
                StartCoroutine(Fadeout(fadeImage, 1.0f));
                break;
        }
    }

    private IEnumerator Fadein(Image fadeinImage, float blackOrWhite) {
        m_alpha = 1.0f;
        m_blackOrWhite = blackOrWhite;
        while (true) {
            if (fadeinImage.color.a <= 0.0f) {
                break;
            }
            fadeinImage.color = new Color(m_blackOrWhite, m_blackOrWhite, m_blackOrWhite, m_alpha);
            m_alpha -= Time.deltaTime;
            yield return null;
        }
    }

    private IEnumerator Fadeout(Image fadeoutImage, float blackOrWhite) {
        m_alpha = 0.0f;
        m_blackOrWhite = blackOrWhite;
        while (true) {
            if (fadeoutImage.color.a >= 1.0f) {
                break;
            }
            fadeoutImage.color = new Color(m_blackOrWhite, m_blackOrWhite, m_blackOrWhite, m_alpha);
            m_alpha += Time.deltaTime;
            yield return null;
        }
    }
}
