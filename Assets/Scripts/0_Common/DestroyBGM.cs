﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBGM : MonoBehaviour {

    private GameObject m_bgmObj;
    public float m_volumeDownSpeed;

    public void OnClick() {
        StartCoroutine(DestroyBGMObject());
    }

    private IEnumerator DestroyBGMObject() {
        DontDestroy[] dd = GameObject.FindObjectsOfType<DontDestroy>();
        AudioSource bgmSource;

        foreach(var d in dd) {
            if(d.name == "BGM") {
                m_bgmObj = d.gameObject;
                break;
            }
        }

        bgmSource = m_bgmObj.GetComponent<AudioSource>();
        while(bgmSource.volume > 0) {
            bgmSource.volume -= Time.deltaTime * m_volumeDownSpeed;
            yield return null;
        }
        Destroy(m_bgmObj.gameObject);
    }
}
