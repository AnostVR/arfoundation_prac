﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class SceneLoader : MonoBehaviour {

    private AsyncOperation m_async;
    private Image m_fadeoutImage;
    [SerializeField] private string m_sceneName;
    [SerializeField] private Image m_fadeImage;
    [SerializeField] private Fading m_fading;
    public Fading.FadeType m_fadeType = Fading.FadeType.OUT_BLACK;
    private const float WAIT_FOR_FADING = 1.5f;

    void Start() {
        // preload scene
        m_async = SceneManager.LoadSceneAsync(m_sceneName);
        m_async.allowSceneActivation = false;
    }
        
    public void OnClick() {
        StartCoroutine(LoadScene());
    }

    public IEnumerator LoadScene() {
        m_fading.FadeStart(m_fadeType, m_fadeImage);
        while (m_async.progress < 0.9f) {
            yield return null;
        }
        // 最低でも1.5秒はフェードアウトのために待つ
        yield return new WaitForSeconds(WAIT_FOR_FADING);
        m_async.allowSceneActivation = true;   
    }

    public IEnumerator LoadGameOverScene() {
        m_fading.FadeStart(m_fadeType, m_fadeImage);
        while(m_async.progress < 0.9f) {
            yield return null;
        }
        ARSessionOrigin arSessionOrigin = GameObject.FindObjectOfType<ARSessionOrigin>();
        ARSession arSession = GameObject.FindObjectOfType<ARSession>();
        Destroy(arSessionOrigin.gameObject);
        Destroy(arSession.gameObject);
        yield return new WaitForSeconds(WAIT_FOR_FADING);
        m_async.allowSceneActivation = true;
    }
}
