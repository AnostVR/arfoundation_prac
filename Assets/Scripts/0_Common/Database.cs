﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Database : MonoBehaviour {

    [SerializeField] private int m_point = 100;

    public PetsType[] m_petDatabase;
    [SerializeField] private PetsType m_selectedPet;

    public void AddPoint(int point) {
        m_point += point;
    }

    public int GetPoint() {
        return m_point;
    }

    public void SetPet(string selectedPetName) {
        foreach(var pet in m_petDatabase) {
            if(pet.name == selectedPetName) {
                m_selectedPet = pet;
            }
        }
    }

    public PetsType GetPet() {
        return m_selectedPet;
    }
}

[System.Serializable]
public class PetsType {
    public string name;
    public int hp;
    public float moveSpeed;
    public float jumpSpeed;
    public float flightDutation;
}