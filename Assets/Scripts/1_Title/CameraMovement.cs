﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public float m_rotateSpeed;

    void Start() {
        
    }

    
    void Update() {
        transform.RotateAround(Vector3.zero, Vector3.up, Time.deltaTime * m_rotateSpeed);
    }
}
